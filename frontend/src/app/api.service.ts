import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  navData: any;
  headers: HttpHeaders;

  constructor(
    public http: HttpClient
  ) {
    this.headers = new HttpHeaders();
    this.headers.append("Accept", 'application/json');
    this.headers.append('Content-Type', 'application/json');
    this.headers.append('Access-Control-Allow-Origin', '*');
    
   }

  addUser(data){
    return this.http.post('http://localhost/Med-2-Go/Backend/register.php', data);
  }

  getUser(){
    return this.http.get('http://localhost/Med-2-Go/Backend/getusers.php');
  }

  verifyUser(data: { email: any; password: any; }){
    return this.http.post('http://localhost/Med-2-Go/Backend/verify.php', data, {responseType: 'text'});
  }

  getCoughColds(){
    return this.http.get('http://localhost/Med-2-Go/Backend/getcoughcolds.php');
  }

  getFever(){
    return this.http.get('http://localhost/Med-2-Go/Backend/getfever.php');
  }

  getBodyMusclePain(){
    return this.http.get('http://localhost/Med-2-Go/Backend/getbodymusclepain.php');
  }

  getGut(){
    return this.http.get('http://localhost/Med-2-Go/Backend/getgut.php');
  }

  postMercOrder(data){
    return this.http.post('http://localhost/Med-2-Go/Backend/postmercorder.php', data, {responseType: 'text'});
  }

  setnavData(navObj){
    this.navData = navObj
  }

  getnavData(){
    if(!this.navData)
      return 0
    return this.navData;
  }

  getOrder(data: {useremail: any;}){
    return this.http.post('http://localhost/Med-2-Go/Backend/getorder.php', data, {responseType: 'text'});
  }

  getSingleUser(data: {useremail: any;}){
    return this.http.post('http://localhost/Med-2-Go/Backend/getsingleuser.php', data, {responseType: 'text'});
  }
}
