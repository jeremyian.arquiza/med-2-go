import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  name: any;
  email: any;
  password: any;
  
  constructor(
    public _apiService: ApiService,
    public router:Router
  ) { }

  ngOnInit() {
  }

  addUser(){
    
    let data = {
      name: this.name,
      email: this.email,
      password: this.password,
    }

    this._apiService.addUser(data).subscribe((res:any) => {
      this.name = '';
      this.email = '';
      this.password = '';
      alert('REGISTRATION SUCCESS');
      this.redirectToLogin();

    },(error: any) => {
      console.log("ERROR: INCOMPLETE FIELDS");
      alert('REGISTRATION FAILED');
    })
  }

  redirectToLogin(){
    this.router.navigateByUrl('/login');
 }
}
