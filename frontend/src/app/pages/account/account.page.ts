import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  name: any;
  useremail: any;
  info: any;
  user: any = [];

  constructor(

    public http: HttpClient,
    private _apiService: ApiService,
    private router:Router,
    private route: ActivatedRoute
  ) {

    this.info = this._apiService.getnavData();
   }

  ngOnInit() {
  }

  getSingleUser(){
    
    let data = {
      useremail: this.info,
    }

    this._apiService.getSingleUser(data).subscribe((res:any) => {
      this.user = JSON.parse(res);

    },(error: any) => {
      console.log("ERROR ===", error);
      alert('FAILED');
    })
  }

}
