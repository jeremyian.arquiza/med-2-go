import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderformtgpPageRoutingModule } from './orderformtgp-routing.module';

import { OrderformtgpPage } from './orderformtgp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderformtgpPageRoutingModule
  ],
  declarations: [OrderformtgpPage]
})
export class OrderformtgpPageModule {}
