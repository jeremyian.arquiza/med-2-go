import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderformtgpPage } from './orderformtgp.page';

const routes: Routes = [
  {
    path: '',
    component: OrderformtgpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderformtgpPageRoutingModule {}
