import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-south',
  templateUrl: './south.page.html',
  styleUrls: ['./south.page.scss'],
})
export class SouthPage implements OnInit {

  medname: any;
  medpurp: any;
  coughmed: any = [];
  fevermed: any = [];
  bodymed: any = [];
  gutmed: any = [];


  constructor(
    public http: HttpClient,
    private _apiService: ApiService,
    private router:Router,
    private route: ActivatedRoute

    
  ) { }

  ngOnInit() {

    this.getCoughColds();
    this.getFever();
    this.getBodyMusclePain();
    this.getGut();
  }

  getCoughColds(){
    
    let data = {
      medname: this.medname,
      medpurp: this.medpurp
    }

    this._apiService.getCoughColds().subscribe((res:any) => {
      this.coughmed = res;

    },(error: any) => {
      console.log("ERROR ===", error);
    })
  }

  getFever(){
    
    let data = {
      medname: this.medname,
      medpurp: this.medpurp
    }

    this._apiService.getFever().subscribe((res:any) => {
      this.fevermed = res;

    },(error: any) => {
      console.log("ERROR ===", error);
    })
  }

  getBodyMusclePain(){
    
    let data = {
      medname: this.medname,
      medpurp: this.medpurp
    }

    this._apiService.getBodyMusclePain().subscribe((res:any) => {
      this.bodymed = res;

    },(error: any) => {
      console.log("ERROR ===", error);
    })
  }

  getGut(){
    
    let data = {
      medname: this.medname,
      medpurp: this.medpurp
    }

    this._apiService.getGut().subscribe((res:any) => {
      this.gutmed = res;

    },(error: any) => {
      console.log("ERROR ===", error);
    })
  }
}
