import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  name: any;
  email: any;
  password: any;
  user: any = [];
  

  constructor(
    public http: HttpClient,
    private _apiService: ApiService,
    private router:Router,
    private route: ActivatedRoute
    
    ) {
    }

  ngOnInit() {
  }

  

  getUser(){
    
    let data = {
      name: this.name,
      email: this.email,
      password: this.password,
    }

    this._apiService.getUser().subscribe((res:any) => {
      console.log("SUCCESS ===", res);
      this.user = res;
      alert('SUCCESS');

    },(error: any) => {
      console.log("ERROR ===", error);
      alert('FAILED');
    })
  }

  verifyUser(){

    let data = {
      email: this.email,
      password: this.password,
    };

    this._apiService.verifyUser(data).subscribe((res) => {
      if (res == "SUCCESS")
      {
        this._apiService.setnavData(this.email);
        this.redirectToLanding();
      }
      else
      {
        console.log("...");
      }
      

    },(error: any) => {
      console.log("ERROR ===", error);
      alert('FAILED');
    })
  }

  redirectToLanding(){
    this.router.navigateByUrl('/landing');
 }

}
