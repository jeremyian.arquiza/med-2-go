import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TgpPageRoutingModule } from './tgp-routing.module';

import { TgpPage } from './tgp.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TgpPageRoutingModule
  ],
  declarations: [TgpPage]
})
export class TgpPageModule {}
