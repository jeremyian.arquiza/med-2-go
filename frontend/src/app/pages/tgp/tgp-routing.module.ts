import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TgpPage } from './tgp.page';

const routes: Routes = [
  {
    path: '',
    component: TgpPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TgpPageRoutingModule {}
