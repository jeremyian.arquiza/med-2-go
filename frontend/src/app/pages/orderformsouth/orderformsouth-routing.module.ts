import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderformsouthPage } from './orderformsouth.page';

const routes: Routes = [
  {
    path: '',
    component: OrderformsouthPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderformsouthPageRoutingModule {}
