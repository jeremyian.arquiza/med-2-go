import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-orderformsouth',
  templateUrl: './orderformsouth.page.html',
  styleUrls: ['./orderformsouth.page.scss'],
})
export class OrderformsouthPage implements OnInit {

  info: any;
  number = 0;
  useremail : any;
  storecode = "ss";
  address: any;
  mercuryorder1 : string = "1";
  mercquant1 : number = 1;
  mercuryorder2 : string = "N/A";
  mercquant2 : number = 0;
  mercuryorder3 : string = "N/A";
  mercquant3 : number = 0;
  mercuryorder4 : string = "N/A";
  mercquant4 : number = 0;

  constructor(

    public http: HttpClient,
    private _apiService: ApiService,
    private router:Router,
    private route: ActivatedRoute
  ) {

    this.info = this._apiService.getnavData();
   }

  ngOnInit() {

  }

  postMercOrder(){
    
    let data = {
      useremail : this.info,
      storecode : this.storecode,
      address: this.address,
      mercuryorder1 : this.mercuryorder1,
      mercquant1 : this.mercquant1,
      mercuryorder2 : this.mercuryorder2,
      mercquant2 : this.mercquant2,
      mercuryorder3 : this.mercuryorder3,
      mercquant3 : this.mercquant3,
      mercuryorder4 : this.mercuryorder4,
      mercquant4 : this.mercquant4,
    }

    this._apiService.postMercOrder(data).subscribe((res:any) => {
      console.log("SUCCESS ===", res);
      this.info = '';
      this.storecode = '';
      this.address = '';
      this.mercuryorder1 = '';
      this.mercquant1;
      this.mercuryorder2 = '';
      this.mercquant2;
      this.mercuryorder3 = '';
      this.mercquant3;
      this.mercuryorder4 = '';
      this.mercquant4;
      alert('SUCCESS');
      this.redirectToSummary();
      

    },(error: any) => {
      console.log("ERROR ===", error);
      alert('FAILED');
    })
  }

  redirectToSummary(){
    this.router.navigateByUrl('/ordersum');
 }

}
