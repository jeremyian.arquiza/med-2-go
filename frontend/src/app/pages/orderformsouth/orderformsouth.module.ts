import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderformsouthPageRoutingModule } from './orderformsouth-routing.module';

import { OrderformsouthPage } from './orderformsouth.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderformsouthPageRoutingModule
  ],
  declarations: [OrderformsouthPage]
})
export class OrderformsouthPageModule {}
