import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {

  data: any;
  constructor(
    public http: HttpClient,
    private _apiService: ApiService,
    private router:Router,
    private route: ActivatedRoute
  ) { 

    this.data = this._apiService.getnavData();
  }


  ngOnInit() {
  }

}
