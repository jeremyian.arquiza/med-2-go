import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {

  med_one_price: any;
  info: any;
  useremail: any;
  store_code: any;
  address: any;
  med_one: any;
  med_onequant: any;
  med_two: any;
  med_twoquant: any;
  med_three: any;
  med_threequant: any;
  med_four: any;
  med_fourquant: any;
  order: any = [];

  constructor(

    public http: HttpClient,
    private _apiService: ApiService,
    private router:Router,
    private route: ActivatedRoute
  ) { 

    this.info = this._apiService.getnavData();
  }

  ngOnInit() {
    this._apiService.getOrder(this.info);
  }

  getOrder(){

    let data = {
      useremail : this.info,
    }

    this._apiService.getOrder(data).subscribe((res:any) => {
      this.order = JSON.parse(res);

    },(error: any) => {
      console.log("ERROR ===", error);
      alert('FAILED');
    })
  }

}
