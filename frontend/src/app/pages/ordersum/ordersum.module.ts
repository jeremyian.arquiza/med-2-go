import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersumPageRoutingModule } from './ordersum-routing.module';

import { OrdersumPage } from './ordersum.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrdersumPageRoutingModule
  ],
  declarations: [OrdersumPage]
})
export class OrdersumPageModule {}
