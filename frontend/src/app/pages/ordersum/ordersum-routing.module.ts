import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersumPage } from './ordersum.page';

const routes: Routes = [
  {
    path: '',
    component: OrdersumPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersumPageRoutingModule {}
