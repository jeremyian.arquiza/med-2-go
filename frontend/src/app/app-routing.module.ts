import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },

  {
    path: 'landing',
    loadChildren: () => import('./pages/landing/landing.module').then( m => m.LandingPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'orders',
    loadChildren: () => import('./pages/orders/orders.module').then( m => m.OrdersPageModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then( m => m.AccountPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./pages/about/about.module').then( m => m.AboutPageModule)
  },
  {
    path: 'mercury',
    loadChildren: () => import('./pages/mercury/mercury.module').then( m => m.MercuryPageModule)
  },
  {
    path: 'south',
    loadChildren: () => import('./pages/south/south.module').then( m => m.SouthPageModule)
  },
  {
    path: 'tgp',
    loadChildren: () => import('./pages/tgp/tgp.module').then( m => m.TgpPageModule)
  },
  {
    path: 'orderform',
    loadChildren: () => import('./pages/orderform/orderform.module').then( m => m.OrderformPageModule)
  },
  {
    path: 'ordersum',
    loadChildren: () => import('./pages/ordersum/ordersum.module').then( m => m.OrdersumPageModule)
  },
  {
    path: 'orderformsouth',
    loadChildren: () => import('./pages/orderformsouth/orderformsouth.module').then( m => m.OrderformsouthPageModule)
  },
  {
    path: 'orderformtgp',
    loadChildren: () => import('./pages/orderformtgp/orderformtgp.module').then( m => m.OrderformtgpPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
